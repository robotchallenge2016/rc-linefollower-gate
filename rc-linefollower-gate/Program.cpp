#include "Program.h"

#include <stdio.h>
#include <math.h>
#include <string.h>

#include "Application.h"

#include "peripherals/Keyboard.h"
#include "peripherals/Lasers.h"
#include "peripherals/LedDisplay.h"
#include "peripherals/SegmentDisplay.h"
#include "peripherals/Sensors.h"
#include "peripherals/Serial.h"
#include "peripherals/TimerDriver.h"

#include "utils/usart_driver.h"
#include "utils/avr_compiler.h"
#include "utils/delays.h"

#include "wireless/http/HttpClient.h"
#include "wireless/http/HttpServer.h"
#include "wireless/http/HttpRequest.h"
#include "wireless/http/HttpResponse.h"
#include "wireless/WiFi.h"

#include "Animation.h"

Program::Program(HttpClient& httpClient, HttpServer& httpServer, WiFi& wifi) 
	: 
	state(WAITING_FOR_ROBOT), 
	animation(new Animation(segmentDisplay)),
	httpClient(httpClient),
	httpServer(httpServer),
	threshold(100),
	tickCounter(0),
	waitCounter(0),
	waitForResponseCounter(0),
	isManualStart(false),
	isAutomaticStart(false),
	wifi(wifi)
{
	initializeHardware();
}

Program::~Program()
{
	delete animation;
}

void Program::initializeHardware()
{
	// konfiguracja timera
	TCD0.CTRLB		=	TC_WGMODE_SINGLESLOPE_gc;
	TCD0.PER		=	32000;
	TCD0.CTRLA		=	TC_CLKSEL_DIV1_gc;
	TCD0.INTCTRLA	=	TC_OVFINTLVL_LO_gc;
}

void Program::run()
{
	switch(state)
	{
		case WAITING_FOR_ROBOT:
		{
			waitForRobot();
			break;
		}
		case WAITING_FOR_RACE_START:
		{
			waitForRaceStart();
			break;
		}
		case WAITING_FOR_RACE_STOP:
		{
			waitForRaceStop();
			break;
		}
		case SEND_RESULT_TO_SERVER:
		{
			sendResultToServer();
			break;
		}
		case WAITING_FOR_SERVER_RESPONSE:
		{
			waitForResponseFromServer();
			break;
		}
		case SHOW_SUCCESS:
		{
			showSuccess();
			break;
		}
		case SHOW_ERROR:
		{
			showError();
			break;
		}
		case WAITING_AFTER_RACE:
		{
			waitAfterRace();
			break;
		}
	}	
}

void Program::waitForRobot()
{
	if(!manualStart() && !automaticStart())
	{
		checkRightLaser();
		checkLeftLaser();
		isAutomaticStart = isManualStart = false;
	}
	else
	{
		timerDriver.zeroes();
		segmentDisplay.switchToGreen();
		state = WAITING_FOR_RACE_START;
	}
}

bool Program::manualStart()
{
	if(keyboard.isSw0Pressed())
	{
		isManualStart = true;
		return true;
	}
	return false;
}

bool Program::automaticStart()
{
	if(httpServer.isRequestAvailable())
	{
		HttpRequest *request = httpServer.getRequest();
		httpServer.requestHandled();

		const char* body = request->getBody();
		uint32_t contentLength = request->getContentLength();

		uint8_t bodyIdx = 0;
		if(body[bodyIdx] == '{')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		//Competition GUID
		for(uint8_t i = 0; i < strlen(competitionGuidId); ++i)
		{
			if(competitionGuidId[i] == body[bodyIdx])
			{
				++bodyIdx;
			}
		}
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == ':')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		for(uint8_t i = 0; i < 36; ++i)
		{
			competitionGuid[i] = body[bodyIdx++];
		}
		competitionGuid[36] = '\0';

		//Delimiter
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == ',')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		//Robot GUID
		for(uint8_t i = 0; i < strlen(robotGuidId); ++i)
		{
			if(robotGuidId[i] == body[bodyIdx])
			{
				++bodyIdx;
			}
		}
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == ':')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		for(uint8_t i = 0; i < 36; ++i)
		{
			robotGuid[i] = body[bodyIdx++];
		}
		robotGuid[36] = '\0';
		if(body[bodyIdx] == '"')
		{
			++bodyIdx;
		}
		if(body[bodyIdx] == '}')
		{
			++bodyIdx;
		}

		isAutomaticStart = (bodyIdx == contentLength);
		return isAutomaticStart;
	}
	return false;
}

bool Program::checkRightLaser()
{
	uint16_t detectorRefLeft = sensors.getDetectorRefLeft();
	uint16_t detectorLeft = sensors.getDetectorLeft();

	if(abs((int32_t)detectorLeft - detectorRefLeft) < 250)
	{
		ledDisplay.led3On();
		return true;
	}
	else
	{
		ledDisplay.led3Off();
		return false;
	}
}

bool Program::checkLeftLaser()
{
	uint16_t detectorRefRight = sensors.getDetectorRefRight();
	uint16_t detectorRight = sensors.getDetectorRight();

	if(abs((int32_t)detectorRight - detectorRefRight) < 250)
	{
		ledDisplay.led0On();
		return true;
	}
	else
	{
		ledDisplay.led0Off();
		return false;
	}
}

void Program::waitForRaceStart()
{
	if(!timerDriver.isStarted())
	{
		if(checkRightLaser())
		{
			timerDriver.start();
		}
	}
	else
	{
		state = WAITING_FOR_RACE_STOP;
	}
}

void Program::waitForRaceStop()
{
	if(timerDriver.isStarted())
	{
		if(!timerDriver.isInDeadZone())
		{
			if(checkRightLaser())
			{
				timerDriver.stop();
			}
		}
		timerDriver.showTime();
	}
	else
	{
		if(isAutomaticStart)
		{
			state = SEND_RESULT_TO_SERVER;
		}
		else
		{
			segmentDisplay.switchToRed();
			state = WAITING_AFTER_RACE;
		}
	}
}

void Program::sendResultToServer()
{
	char buffer[192];
	const char* bodyTemplate = 
	"{\"result\":%lu,\"competitionGuid\":\"%s\",\"robotGuid\":\"%s\"}";
	snprintf(buffer, 192, bodyTemplate, timerDriver.getCounter(), competitionGuid, robotGuid);

	HttpRequest request("POST", "/rc-webapp/v1/tournament/result/linefollower/gate/", buffer);
	httpClient.send(request);

	waitForResponseCounter = 0;
	state = WAITING_FOR_SERVER_RESPONSE;
}

void Program::waitForResponseFromServer()
{
	if(httpClient.isResponseAvailable())
	{
		HttpResponse *response = httpClient.getResponse();
		if(response->getStatusCode() == 201)
		{
			serial.print("201 OK\r\n");
			state = SHOW_SUCCESS;
			successCounter = 0;
		}
		else if(response->getStatusCode() == 400)
		{
			serial.print("400 Bad Request\r\n");
			state = SHOW_ERROR;
		}
		else
		{
			char text[64];
			snprintf(text, 64, "Something bad happened %u\r\n", response->getStatusCode());
			serial.print(text);
			state = SHOW_ERROR;
		}
	}
	else if(waitForResponseCounter > 1000)
	{
		state = SHOW_ERROR;
	}
}

void Program::showSuccess()
{
	if(successCounter <= 500)
	{
		animation->success();
	}
	else
	{
		segmentDisplay.switchToRed();
		timerDriver.showTime();
		state = WAITING_AFTER_RACE;
	}
}

void Program::showError()
{
	if(errorCounter <= 1000)
	{
		segmentDisplay.switchToRed();
		timerDriver.showTime();
	}
	else if(errorCounter > 1000 && errorCounter < 2000)
	{
		animation->error();
	}
	else
	{
		errorCounter = 0;
	}
}

void Program::waitAfterRace()
{
	if(keyboard.isSw1Pressed() || httpServer.isRequestAvailable() || waitCounter >= 60000)
	{
		waitCounter = 0;
		state = WAITING_FOR_ROBOT;
	}
}

void Program::tick()
{
	++tickCounter;
	if(tickCounter >= threshold)
	{
		tickCounter = 0;

		ledDisplay.toggleLed1();
		if(state == WAITING_FOR_ROBOT)
		{
			segmentDisplay.switchToYellow();
			if(wifi.isReady())
			{
				animation->play();
			}
			else
			{
				animation->boot();
			}
		}
	}
	if(state == WAITING_AFTER_RACE)
	{
		++waitCounter;
	}
	if(state == WAITING_FOR_SERVER_RESPONSE)
	{
		++waitForResponseCounter;
	}
	if(state == SHOW_SUCCESS)
	{
		++successCounter;
	}
	if(state == SHOW_ERROR)
	{
		++errorCounter;
	}
}

ISR(TCD0_OVF_vect)
{
	program.tick();
}