#include "LedDisplay.h"
#include "utils/avr_compiler.h"

LedDisplay ledDisplay;

LedDisplay::LedDisplay()
{
	initializeHardware();
	allOff();
}

LedDisplay::~LedDisplay()
{
}

void LedDisplay::initializeHardware()
{
	PORTC.DIRSET = LED0 | LED1 | LED2 | LED3;
}

void LedDisplay::led0On()
{
	PORTC.OUTCLR = LED0;
}

void LedDisplay::led0Off()
{
	PORTC.OUTSET = LED0;
}

void LedDisplay::toggleLed0()
{
	PORTC.OUTTGL = LED0;
}

void LedDisplay::led1On()
{
	PORTC.OUTCLR = LED1;
}

void LedDisplay::led1Off()
{
	PORTC.OUTSET = LED1;
}

void LedDisplay::toggleLed1()
{
	PORTC.OUTTGL = LED1;
}

void LedDisplay::led2On()
{
	PORTC.OUTCLR = LED2;
}

void LedDisplay::led2Off()
{
	PORTC.OUTSET = LED2;
}

void LedDisplay::toggleLed2()
{
	PORTC.OUTTGL = LED2;
}

void LedDisplay::led3On()
{
	PORTC.OUTCLR = LED3;
}

void LedDisplay::led3Off()
{
	PORTC.OUTSET = LED3;
}

void LedDisplay::toggleLed3()
{
	PORTC.OUTTGL = LED3;
}

void LedDisplay::allOn()
{
	PORTC.OUTCLR = LED0 | LED1 | LED2 | LED3;
}

void LedDisplay::allOff()
{
	PORTC.OUTSET = LED0 | LED1 | LED2 | LED3;
}
