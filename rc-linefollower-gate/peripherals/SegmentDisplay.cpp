#include "SegmentDisplay.h"
#include "utils/avr_compiler.h"

SegmentDisplay segmentDisplay;

SegmentDisplay::SegmentDisplay()
{
	initializeHardware();
	switchToYellow();
}

SegmentDisplay::~SegmentDisplay()
{
}

void SegmentDisplay::initializeHardware()
{
	PORTE.DIRSET = RED_K_bm | GREEN_K_bm | DS_bm | SHCP_bm | EN_bm;
	PORTF.DIRSET = MR_bm | STCP_bm;

	turnOff();
	PORTF.OUTSET = MR_bm;

	// konfiguracja timera
    TCE0.CTRLB        =    TC_WGMODE_SINGLESLOPE_gc | TC0_CCAEN_bm | TC0_CCBEN_bm;
    TCE0.PER          =    1000;
    TCE0.CCA          =    100;
    TCE0.CCB          =    900;
    TCE0.CTRLA        =    TC_CLKSEL_DIV1_gc;
}

void SegmentDisplay::turnOn()
{
	PORTE.OUTCLR = EN_bm;
}

void SegmentDisplay::turnOff()
{
	PORTE.OUTSET = EN_bm;
}

void SegmentDisplay::writeAndDisplay(uint8_t *values, uint8_t length)
{
	for(int8_t display = length - 1; display >= 0; --display)
	{
		uint8_t value = values[display];
		for(int8_t i = 7; i >= 0; --i)
		{
			if(value & (1 << i))
			{
				PORTE.OUTSET = DS_bm;
			}
			else
			{
				PORTE.OUTCLR = DS_bm;
			}
			PORTF.OUTSET = STCP_bm;
			PORTF.OUTCLR = STCP_bm;
		}
	}
	
	PORTE.OUTSET = SHCP_bm;
	PORTE.OUTCLR = SHCP_bm;
}

void SegmentDisplay::switchToRed()
{
	TCE0.CCA          =    1000;
	TCE0.CCB          =    0;
}

void SegmentDisplay::switchToGreen()
{
	TCE0.CCA          =    0;
	TCE0.CCB          =    1000;
}

void SegmentDisplay::switchToYellow()
{
	TCE0.CCA          =    150;
	TCE0.CCB          =    850;
}
