#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

class Keyboard;
extern Keyboard keyboard;

class Keyboard
{
private:

public:
	Keyboard();
	~Keyboard();

	bool isSw0Pressed();
	bool isSw1Pressed();

private:
	Keyboard( const Keyboard &c );
	Keyboard& operator=( const Keyboard &c );

	void initializeHardware();
};

#endif //__KEYBOARD_H__
