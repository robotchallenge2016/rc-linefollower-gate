#ifndef __LASERS_H__
#define __LASERS_H__

class Lasers;
extern Lasers lasers;

class Lasers
{
private:

public:
	Lasers();
	~Lasers();

	void turnOnLeft();
	void turnOnRight();
	void turnOffBoth();

private:
	Lasers( const Lasers &c );
	Lasers& operator=( const Lasers &c );

	void initializeHardware();
};

#endif //__LASERS_H__
