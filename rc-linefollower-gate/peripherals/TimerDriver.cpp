#include "TimerDriver.h"
#include "SegmentDisplay.h"

#include "utils/avr_compiler.h"

TimerDriver timerDriver(segmentDisplay);

TimerDriver::TimerDriver(SegmentDisplay &segmentDisplay) 
	: 
	counter(0), 
	hundredsOfThousands(0),
	dozensOfThousands(0),
	thousands(0),
	hundreds(0),
	dozens(0),
	units(0),
	started(false),
	segmentDisplay(segmentDisplay)
{
	initializeHardware();
	segmentDisplay.turnOff();
}

TimerDriver::~TimerDriver()
{
}

void TimerDriver::initializeHardware()
{
	// konfiguracja timera
	TCE1.CTRLB		=	TC_WGMODE_SINGLESLOPE_gc;
	TCE1.PER		=	32000;
	TCE1.CTRLA		=	TC_CLKSEL_DIV1_gc;
	TCE1.INTCTRLA	=	TC_OVFINTLVL_LO_gc;
}

void TimerDriver::start()
{
	clear();
	segmentDisplay.switchToGreen();
	started = true;
}

void TimerDriver::stop()
{
	//segmentDisplay.switchToRed();
	started = false;
}

bool TimerDriver::isStarted()
{
	return started;
}

void TimerDriver::showTime()
{
	decomposeCounter();
	uint8_t values[] =
	{
		digits[units],
		digits[dozens],
		digits[hundreds],
		digits[thousands],
		digits[dozensOfThousands],
		digits[hundredsOfThousands]
	};
	writeToDisplay(values);
}

void TimerDriver::decomposeCounter()
{
	hundredsOfThousands = counter / 100000;
	dozensOfThousands = (counter % 100000) / 10000;
	thousands = (counter % 10000) / 1000;
	hundreds = (counter % 1000) / 100;
	dozens = (counter % 100) / 10;
	units = (counter % 10);
}

bool TimerDriver::isInDeadZone()
{
	decomposeCounter();
	return started && (thousands < 1);
}

void TimerDriver::zeroes()
{
	uint8_t values[6] = { digits[0], digits[0], digits[0], digits[0], digits[0], digits[0]};
	writeToDisplay(values);
}

void TimerDriver::writeToDisplay(uint8_t *values)
{
	values[3] = values[3] | 0x80;
	segmentDisplay.writeAndDisplay(values, 6);
	segmentDisplay.writeAndDisplay(values, 6);
	segmentDisplay.turnOn();
}

void TimerDriver::clear()
{
	counter = 0;
}

uint32_t TimerDriver::getCounter()
{
	return counter;
}

void TimerDriver::tick()
{
	if(started)
	{
		++counter;
		if(counter >= 1000000)
		{
			counter = 0;
		}
	}
}

ISR(TCE1_OVF_vect)
{
	timerDriver.tick();
}