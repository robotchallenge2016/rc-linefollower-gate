#ifndef __LEDDISPLAY_H__
#define __LEDDISPLAY_H__

#define LED0	PIN4_bm
#define LED1	PIN5_bm
#define LED2	PIN6_bm
#define LED3	PIN7_bm

class LedDisplay;
extern LedDisplay ledDisplay;

class LedDisplay
{
private:

public:
	LedDisplay();
	~LedDisplay();

	void allOn();
	void allOff();

	void led0On();
	void led0Off();
	void toggleLed0();
	void led1On();
	void led1Off();
	void toggleLed1();
	void led2On();
	void led2Off();
	void toggleLed2();
	void led3On();
	void led3Off();
	void toggleLed3();

private:
	LedDisplay( const LedDisplay &c );
	LedDisplay& operator=( const LedDisplay &c );

	void initializeHardware();
};

#endif //__LEDDISPLAY_H__
