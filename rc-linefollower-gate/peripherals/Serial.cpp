#include "Serial.h"
#include "utils/avr_compiler.h"
#include "utils/usart_driver.h"

Serial serial;

/*! Define that selects the Usart used in example. */
#define USART_PC	USARTD0

/*! USART data struct used in example. */
USART_data_t USART_PC_data;

Serial::Serial()
{
	initializeHardware();
}

Serial::~Serial()
{
}

void Serial::initializeHardware()
{
	PORTD.DIRSET   = PIN3_bm;
	PORTD.DIRCLR   = PIN2_bm;
	/* Use USARTC0 and initialize buffers. */
	USART_InterruptDriver_Initialize(&USART_PC_data, &USART_PC, USART_DREINTLVL_LO_gc);
	/* USARTC0, 8 Data bits, No Parity, 1 Stop bit. */
	USART_Format_Set(USART_PC_data.usart, USART_CHSIZE_8BIT_gc, USART_PMODE_DISABLED_gc, false);
	/* Enable RXC interrupt. */
	USART_RxdInterruptLevel_Set(USART_PC_data.usart, USART_RXCINTLVL_LO_gc);
	// 32 MHz
	USART_Baudrate_Set(&USART_PC, 17, 0);

	/* Enable both RX and TX. */
	USART_Rx_Enable(USART_PC_data.usart);
	USART_Tx_Enable(USART_PC_data.usart);
}

void Serial::print(const char *s)
{
	while(*s)
	{
		while(!USART_TXBuffer_FreeSpace(&USART_PC_data));
		USART_TXBuffer_PutByte(&USART_PC_data, *s);
		++s;
	}
}

ISR(USARTD0_RXC_vect)
{
	USART_RXComplete(&USART_PC_data);
}

ISR(USARTD0_DRE_vect)
{
	USART_DataRegEmpty(&USART_PC_data);
}
