#ifndef __SENSORS_H__
#define __SENSORS_H__

#include <inttypes.h>

class Sensors;
extern Sensors sensors;

class Sensors
{
private:
	volatile uint16_t detectorRight;
	volatile uint16_t detectorRefRight;
	volatile uint16_t detectorRefLeft;
	volatile uint16_t detectorLeft;

public:
	Sensors();
	~Sensors();

	bool isReady();

	void triggerChannel0();
	void triggerChannel1();
	void triggerChannel2();
	void triggerChannel3();

	void updateDetectorRight(uint16_t value);
	void updateDetectorRefRight(uint16_t value);
	void updateDetectorRefLeft(uint16_t value);
	void updateDetectorLeft(uint16_t value);

	uint16_t getDetectorRight();
	uint16_t getDetectorRefRight();
	uint16_t getDetectorRefLeft();
	uint16_t getDetectorLeft();

private:
	Sensors( const Sensors &c );
	Sensors& operator=( const Sensors &c );

	void initializeHardware();
};

#endif //__SENSORS_H__
