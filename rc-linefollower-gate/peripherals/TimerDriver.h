#ifndef __TIMERDRIVER_H__
#define __TIMERDRIVER_H__

#include <inttypes.h>

class SegmentDisplay;
class TimerDriver;
extern TimerDriver timerDriver;

class TimerDriver
{
private:
	uint8_t digits[10] =
	{
		0b00111111,
		0b00000110,
		0b01011011,
		0b01001111,
		0b01100110,
		0b01101101,
		0b01111101,
		0b00000111,
		0b01111111,
		0b01101111
	};
	uint32_t counter;
	uint8_t hundredsOfThousands;
	uint8_t dozensOfThousands;
	uint8_t thousands;
	uint8_t hundreds;
	uint8_t dozens;
	uint8_t units;
	volatile bool started;
	SegmentDisplay &segmentDisplay;

public:
	TimerDriver(SegmentDisplay &segmentDisplay);
	~TimerDriver();

	void start();
	void stop();
	bool isStarted();

	void showTime();
	void decomposeCounter();
	bool isInDeadZone();

	void zeroes();
	void tick();
	void clear();

	uint32_t getCounter();
private:
	TimerDriver( const TimerDriver &c );
	TimerDriver& operator=( const TimerDriver &c );

	void initializeHardware();
	void writeToDisplay(uint8_t *values);
};

#endif //__TIMERDRIVER_H__
