#ifndef __SEGMENTDISPLAY_H__
#define __SEGMENTDISPLAY_H__

#include <inttypes.h>

// PORTE
#define	RED_K_bm	PIN0_bm
#define	GREEN_K_bm	PIN1_bm

#define EN_bm		PIN4_bm
#define DS_bm		PIN5_bm
#define SHCP_bm		PIN7_bm


// PORTF
#define MR_bm		PIN4_bm
#define STCP_bm		PIN5_bm

class SegmentDisplay;
extern SegmentDisplay segmentDisplay;

class SegmentDisplay
{
private:

public:
	SegmentDisplay();
	~SegmentDisplay();

	void writeAndDisplay(uint8_t *values, uint8_t length);
	void turnOn();
	void turnOff();

	void switchToRed();
	void switchToGreen();
	void switchToYellow();

private:
	SegmentDisplay( const SegmentDisplay &c );
	SegmentDisplay& operator=( const SegmentDisplay &c );

	void initializeHardware();
};

#endif //__SEGMENTDISPLAY_H__
