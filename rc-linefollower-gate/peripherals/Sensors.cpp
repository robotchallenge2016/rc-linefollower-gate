#include "Sensors.h"
#include "utils/avr_compiler.h"

Sensors sensors;

Sensors::Sensors()
{
	initializeHardware();
	triggerChannel0();
}

Sensors::~Sensors()
{
}

void Sensors::initializeHardware()
{
	// ADC
	ADCA.CTRLA = ADC_ENABLE_bm;
	ADCA.REFCTRL = ADC_REFSEL1_bm;
	ADCA.PRESCALER = ADC_PRESCALER2_bm | ADC_PRESCALER1_bm | ADC_PRESCALER0_bm;
	ADCA.EVCTRL = ADC_SWEEP1_bm | ADC_SWEEP0_bm;
	
	ADCA.CH0.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH0.MUXCTRL = ADC_CH_MUXINT2_bm;
	ADCA.CH0.INTCTRL = ADC_CH_INTLVL0_bm;
	
	ADCA.CH1.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH1.MUXCTRL = ADC_CH_MUXINT2_bm | ADC_CH_MUXINT0_bm;
	ADCA.CH1.INTCTRL = ADC_CH_INTLVL0_bm;

	ADCA.CH2.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH2.MUXCTRL = ADC_CH_MUXINT2_bm | ADC_CH_MUXINT1_bm;
	ADCA.CH2.INTCTRL = ADC_CH_INTLVL0_bm;

	ADCA.CH3.CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;
	ADCA.CH3.MUXCTRL = ADC_CH_MUXINT2_bm | ADC_CH_MUXINT1_bm | ADC_CH_MUXINT0_bm;
	ADCA.CH3.INTCTRL = ADC_CH_INTLVL0_bm;
}

bool Sensors::isReady()
{
	return (detectorRight > 0) && 
			(detectorRefRight > 0) &&
			(detectorRefLeft > 0) &&
			(detectorLeft > 0);
}

void Sensors::triggerChannel0()
{
	ADCA.CH0.CTRL |= ADC_CH_START_bm;
}

void Sensors::triggerChannel1()
{
	ADCA.CH1.CTRL |= ADC_CH_START_bm;
}

void Sensors::triggerChannel2()
{
	ADCA.CH2.CTRL |= ADC_CH_START_bm;
}

void Sensors::triggerChannel3()
{
	ADCA.CH3.CTRL |= ADC_CH_START_bm;
}

void Sensors::updateDetectorRight(uint16_t value)
{
	detectorRight = value;
}

void Sensors::updateDetectorRefRight(uint16_t value)
{
	detectorRefRight = value;
}

void Sensors::updateDetectorRefLeft(uint16_t value)
{
	detectorRefLeft = value;
}

void Sensors::updateDetectorLeft(uint16_t value)
{
	detectorLeft = value;
}

uint16_t Sensors::getDetectorRight()
{
	return detectorRight;
}

uint16_t Sensors::getDetectorRefRight()
{
	return detectorRefRight;
}

uint16_t Sensors::getDetectorRefLeft()
{
	return detectorRefLeft;
}

uint16_t Sensors::getDetectorLeft()
{
	return detectorLeft;
}

ISR(ADCA_CH0_vect)
{
	sensors.updateDetectorRight(ADCA.CH0.RES);
	sensors.triggerChannel1();
}

ISR(ADCA_CH1_vect)
{
	sensors.updateDetectorRefRight(ADCA.CH1.RES);
	sensors.triggerChannel2();
}

ISR(ADCA_CH2_vect)
{
	sensors.updateDetectorRefLeft(ADCA.CH2.RES);
	sensors.triggerChannel3();
}

ISR(ADCA_CH3_vect)
{
	sensors.updateDetectorLeft(ADCA.CH3.RES);
	sensors.triggerChannel0();
}
