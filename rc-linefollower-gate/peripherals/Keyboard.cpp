#include "Keyboard.h"
#include "utils/avr_compiler.h"

Keyboard keyboard;

Keyboard::Keyboard()
{
	initializeHardware();
}

Keyboard::~Keyboard()
{
}

void Keyboard::initializeHardware()
{
	PORTC.DIRCLR = PIN0_bm | PIN1_bm;
	PORTC.PIN0CTRL = PORT_OPC_PULLUP_gc;
	PORTC.PIN1CTRL = PORT_OPC_PULLUP_gc;
}

bool Keyboard::isSw0Pressed()
{
	return !(PORTC.IN & PIN0_bm);
}

bool Keyboard::isSw1Pressed()
{
	return !(PORTC.IN & PIN1_bm);
}
