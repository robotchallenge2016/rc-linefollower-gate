#ifndef __SERIAL_H__
#define __SERIAL_H__

#include <inttypes.h>

class Serial;
extern Serial serial;

#include "utils/usart_driver.h"
extern USART_data_t USART_PC_data;

class Serial
{
private:

public:
	Serial();
	~Serial();

	void print(const char *s);

private:
	Serial( const Serial &c );
	Serial& operator=( const Serial &c );

	void initializeHardware();
};

#endif //__SERIAL_H__
