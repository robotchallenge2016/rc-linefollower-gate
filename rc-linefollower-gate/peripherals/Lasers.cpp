#include "Lasers.h"
#include "utils/avr_compiler.h"

Lasers lasers;

Lasers::Lasers()
{
	initializeHardware();
}

Lasers::~Lasers()
{
}

void Lasers::initializeHardware()
{
	PORTA.DIRSET = PIN1_bm | PIN2_bm;
}

void Lasers::turnOnLeft()
{
	PORTA.OUTSET = PIN2_bm;
}

void Lasers::turnOnRight()
{
	PORTA.OUTSET = PIN1_bm;
}

void Lasers::turnOffBoth()
{
	PORTA.OUTCLR = PIN1_bm | PIN2_bm;
}