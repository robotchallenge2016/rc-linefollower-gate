#include "utils/avr_compiler.h"

#include "Application.h"

#include "peripherals/Lasers.h"
#include "peripherals/Sensors.h"
#include "peripherals/Serial.h"
#include "peripherals/TimerDriver.h"

#include "wireless/http/HttpClient.h"
#include "wireless/http/HttpServer.h"
#include "wireless/WiFi.h"
#include "wireless/WiFiDataRouter.h"

#include "Program.h"

#include <stdio.h>

void Osc32MHz(void) {
	OSC.CTRL          =    OSC_RC32MEN_bm;              // w��czenie oscylatora 32MHz
	while(!(OSC.STATUS & OSC_RC32MRDY_bm));             // czekanie na ustabilizowanie si� generatora
	CPU_CCP           =    CCP_IOREG_gc;                // odblokowanie zmiany �r�d�a sygna�u
	CLK.CTRL          =    CLK_SCLKSEL_RC32M_gc;        // zmiana �r�d�a sygna�u zegarowego na RC 32MHz
}

void uCSetup()
{
	Osc32MHz();
	/* Enable PMIC interrupt level low. */
	PMIC.CTRL |= PMIC_LOLVLEX_bm;
	/* Enable global interrupts. */
	sei();
}

void programSetup()
{
	while(!sensors.isReady());
	//lasers.turnOnLeft();
	lasers.turnOnRight();
}

void forwardSerialDataToWiFi()
{
	if(USART_RXBufferData_Available(&USART_PC_data))
	{
		uint8_t receivedData = USART_RXBuffer_GetByte(&USART_PC_data);
		if(receivedData == 0x0D)
		{
			USART_TXBuffer_PutByte(&USART_WIFI_data, '\r');
			USART_TXBuffer_PutByte(&USART_WIFI_data, '\n');
		}
		else
		{
			USART_TXBuffer_PutByte(&USART_WIFI_data, receivedData);
		}
	}
}

int main(void)
{
	uCSetup();
	programSetup();

	while(1)
	{
		dataRouter.route();
		httpClient.work();
		httpServer.work();
		wifi.work();
		//forwardSerialDataToWiFi();
		if(wifi.isReady())
		{
			program.run();
		}
	}
}
