#ifndef __READYPARSER_H__
#define __READYPARSER_H__

#include "wireless/Parser.h"

class ReadyParser : public Parser
{
private:
	uint8_t index;
public:
	ReadyParser();
	~ReadyParser();

	void parse(uint8_t data);

private:
	ReadyParser( const ReadyParser &c );
	ReadyParser& operator=( const ReadyParser &c );

	void validate(uint8_t data, uint8_t expectedValue, uint8_t nextIndex);
};

#endif //__READYPARSER_H__
