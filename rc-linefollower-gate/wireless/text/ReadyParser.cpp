#include "ReadyParser.h"

ReadyParser::ReadyParser() : index(0)
{

}

ReadyParser::~ReadyParser()
{

}

void ReadyParser::parse(uint8_t data)
{
	inProgress();
	switch(index)
	{
		case 0:
		{
			validate(data, 'r', 1);
			break;
		}
		case 1:
		{
			validate(data, 'e', 2);
			break;
		}
		case 2:
		{
			validate(data, 'a', 3);
			break;
		}
		case 3:
		{
			validate(data, 'd', 4);
			break;
		}
		case 4:
		{
			validate(data, 'y', 0);
			success();
			break;
		}
	}
}

void ReadyParser::validate(uint8_t data, uint8_t expectedValue, uint8_t nextIndex)
{
	if(data == expectedValue)
	{
		index = nextIndex;
	}
	else
	{
		index = 0;
	}
}
