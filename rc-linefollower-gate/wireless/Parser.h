#ifndef __ATPARSER_H__
#define __ATPARSER_H__

#include <inttypes.h>

enum ParsingState
{
	NOT_STARTED,
	IN_PROGRESS,
	SUCCESS,
	ERROR
};

class Parser
{
private:
	ParsingState state;
protected:
	uint8_t asciiOffset = 48;
public:
	Parser();
	virtual ~Parser();
	virtual void parse(uint8_t data) = 0;

	void rewind();
	void inProgress();
	void success();
	void error();

	bool isFinished();
	bool isSuccess();
	bool isError();
private:
	void changeStateTo(ParsingState state);
};

#endif //__ATPARSER_H__
