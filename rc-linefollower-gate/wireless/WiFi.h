#ifndef __WIFI_H__
#define __WIFI_H__

#define USART_WIFI	USARTD1
#define WIFI_PORT	PORTD
#define WIFI_RST	PIN4_bm
#define WIFI_CH_PD	PIN5_bm

class Command;
class CommandFactory;
class Connection;
class Parser;

#include "utils/usart_driver.h"
extern USART_data_t USART_WIFI_data;

class WiFi
{
private:
	bool isStarted;
	bool isInitialized;

	CommandFactory& commandFactory;
	Command* *initializationCommands;
	uint8_t initCmdIndex;
	uint8_t initCmdNumber;

	Parser *readyParser;

	uint8_t cmdIndex;
	uint8_t maxIndex;
	uint8_t	queueSize;
	Command* *queue;

public:
	WiFi(CommandFactory& commandFactory);
	~WiFi();

	bool isReady();
	void work();
	void startAndSend(Connection& connection, const char* raw);
	void send(Connection& connection, const char* raw);
	void parse(uint8_t data);

private:
	WiFi( const WiFi &c );
	WiFi& operator=( const WiFi &c );

	void initializeHardware();
	void initializeWiFiPins();
	void initializeUsart();
	void initializeWiFiModule();
};

#endif //__WIFI_H__
