#include "WiFi.h"
#include "utils/avr_compiler.h"
#include "utils/usart_driver.h"

#include "wireless/at/Command.h"
#include "wireless/at/CommandDispatcher.h"
#include "wireless/at/CommandFactory.h"
#include "wireless/http/Connection.h"
#include "wireless/text/ReadyParser.h"

#include <string.h>

USART_data_t USART_WIFI_data;

WiFi::WiFi(CommandFactory& _commandFactory) 
	: 
	commandFactory(_commandFactory), initCmdIndex(0), initCmdNumber(8), 
	cmdIndex(0), maxIndex(0), queueSize(5)
{
	readyParser = new ReadyParser();
	initializationCommands = new Command*[initCmdNumber];
	initializationCommands[0] = commandFactory.at();
	initializationCommands[1] = commandFactory.at();
	initializationCommands[2] = commandFactory.at();
	initializationCommands[3] = commandFactory.cwmode();
	initializationCommands[4] = commandFactory.cwjap();
	initializationCommands[5] = commandFactory.cifsr();
	initializationCommands[6] = commandFactory.cipmux();
	initializationCommands[7] = commandFactory.cipserver();

	queue = new Command*[queueSize];

	initializeHardware();
}

WiFi::~WiFi()
{
	delete readyParser;
}

void WiFi::initializeHardware()
{
	initializeWiFiPins();
	initializeUsart();
}

void WiFi::initializeWiFiPins()
{
	WIFI_PORT.DIRSET = WIFI_RST | WIFI_CH_PD;
	WIFI_PORT.OUTSET = WIFI_RST | WIFI_CH_PD;
}

void WiFi::initializeUsart()
{
	PORTD.DIRSET   = PIN7_bm;
	PORTD.DIRCLR   = PIN6_bm;

	/* Use USARTD1 and initialize buffers. */
	USART_InterruptDriver_Initialize(&USART_WIFI_data, &USART_WIFI, USART_DREINTLVL_LO_gc);
	/* USARTD1, 8 Data bits, No Parity, 1 Stop bit. */
	USART_Format_Set(USART_WIFI_data.usart, USART_CHSIZE_8BIT_gc, USART_PMODE_DISABLED_gc, false);
	/* Enable RXC interrupt. */
	USART_RxdInterruptLevel_Set(USART_WIFI_data.usart, USART_RXCINTLVL_LO_gc);
	// 32 MHz
	USART_Baudrate_Set(&USART_WIFI, 17, 0);
	/* Enable both RX and TX. */
	USART_Rx_Enable(USART_WIFI_data.usart);
	USART_Tx_Enable(USART_WIFI_data.usart);
}

void WiFi::initializeWiFiModule()
{
	if(!commandDispatcher.isInProgress())
	{
		if(initCmdIndex < initCmdNumber)
		{
			commandDispatcher.send(initializationCommands[initCmdIndex++]);
		}
		else
		{
			isInitialized = true;
		}
	}
}

bool WiFi::isReady()
{
	return isStarted && isInitialized;
}

void WiFi::work()
{
	if(isStarted && !isInitialized)
	{
		initializeWiFiModule();
	}
	else
	{
		if(!commandDispatcher.isInProgress())
		{
			if(cmdIndex < maxIndex)
			{
				commandDispatcher.send(queue[cmdIndex++]);
			}
		}
	}
}

void WiFi::startAndSend(Connection& connection, const char* raw)
{
	queue[0] = commandFactory.cipStart(connection.getId(), 192, 168, 1, 105, 8080);
	queue[1] = commandFactory.cipSend(connection.getId(), strlen(raw));
	queue[2] = commandFactory.data(raw);
	cmdIndex = 0;
	maxIndex = 3;
}

void WiFi::send(Connection& connection, const char* raw)
{
	queue[0] = commandFactory.cipSend(connection.getId(), strlen(raw));
	queue[1] = commandFactory.data(raw);
	queue[2] = commandFactory.cipClose(connection.getId());
	cmdIndex = 0;
	maxIndex = 3;
}

void WiFi::parse(uint8_t data)
{
	readyParser->parse(data);
	if(readyParser->isFinished())
	{
		if(readyParser->isSuccess())
		{
			isStarted = true;
		}
		readyParser->rewind();
	}
}

ISR(USARTD1_RXC_vect)
{
	USART_RXComplete(&USART_WIFI_data);
}

ISR(USARTD1_DRE_vect)
{
	USART_DataRegEmpty(&USART_WIFI_data);
}