#ifndef __HTTPREQUEST_H__
#define __HTTPREQUEST_H__

#include <inttypes.h>

#define HTTP_BUFFER_SIZE 512

class HttpRequest
{
private:
	char buffer[HTTP_BUFFER_SIZE];
	const char* raw;
	char* body;
	uint32_t contentLength;
public:
	HttpRequest(const char* method, const char* url, const char* body);
	HttpRequest(const char* body, uint32_t contentLength);
	~HttpRequest();

	const char* toWire();
	const char* getBody();
	uint32_t getContentLength();

private:
	HttpRequest( const HttpRequest &c );
	HttpRequest& operator=( const HttpRequest &c );
};

#endif //__HTTPREQUEST_H__
