#ifndef __CONNECTION_H__
#define __CONNECTION_H__

#include <inttypes.h>

class Parser;

class Connection
{
private:
	uint8_t id;
	Parser *parser;

	bool closed;
	const char *closedText = "CLOSED";
	uint8_t closedIdx;
	uint8_t maxClosedIdx;
public:
	Connection(uint8_t id);
	~Connection();

	uint8_t getId();
	void responseChunk(uint8_t chunk);

	void setParser(Parser *parser);

	void obtained();
	void released();

	void close();
	bool isClosed();

private:
	Connection( const Connection &c );
	Connection& operator=( const Connection &c );

};

#endif //__CONNECTION_H__
