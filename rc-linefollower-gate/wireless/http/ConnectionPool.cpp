#include "ConnectionPool.h"

#include "Connection.h"

#include <stdlib.h>

ConnectionPool::ConnectionPool() : poolSize(5)
{
	pool = new Connection*[poolSize];
	for(uint8_t i = 0; i < poolSize; ++i)
	{
		pool[i] = new Connection(i);
	}

	status = new ConnectionStatus[poolSize];
	for(uint8_t i = 0; i < poolSize; ++i)
	{
		status[i] = FREE;
	}
}

ConnectionPool::~ConnectionPool()
{
	for(uint8_t i = 0; i < poolSize; ++i)
	{
		delete pool[i];
	}
	delete pool;
	delete status;
}

Connection* ConnectionPool::obtain()
{
	for(uint8_t i = 0; i < poolSize; ++i)
	{
		if(status[i] == FREE)
		{
			status[i] = TAKEN;
			return pool[i];
		}
	}
	return NULL;
}

Connection* ConnectionPool::obtain(uint8_t id)
{
	status[id] = TAKEN;
	pool[id]->obtained();
	return pool[id];
}

void ConnectionPool::release(uint8_t id)
{
	status[id] = FREE;
}
