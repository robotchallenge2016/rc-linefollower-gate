#ifndef __CONNECTIONPOOL_H__
#define __CONNECTIONPOOL_H__

#include <inttypes.h>

class Connection;

enum ConnectionStatus
{
	FREE,
	TAKEN
};

class ConnectionPool
{
private:
	Connection* *pool;
	ConnectionStatus *status;
	uint8_t poolSize;
public:
	ConnectionPool();
	~ConnectionPool();

	Connection* obtain();
	Connection* obtain(uint8_t id);
	void release(uint8_t id);

private:
	ConnectionPool( const ConnectionPool &c );
	ConnectionPool& operator=( const ConnectionPool &c );
};

#endif //__CONNECTIONPOOL_H__
