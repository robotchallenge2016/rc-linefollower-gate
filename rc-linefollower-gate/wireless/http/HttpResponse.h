#ifndef __HTTPRESPONSE_H__
#define __HTTPRESPONSE_H__

#include <inttypes.h>

class HttpResponseParser;

class HttpResponse
{
private:
	uint16_t statusCode;
	char* body;
public:
	HttpResponse(HttpResponseParser& parser);
	HttpResponse( const HttpResponse &c );
	~HttpResponse();

	uint16_t getStatusCode();

protected:
private:
	HttpResponse& operator=( const HttpResponse &c );

};

#endif //__HTTPRESPONSE_H__
