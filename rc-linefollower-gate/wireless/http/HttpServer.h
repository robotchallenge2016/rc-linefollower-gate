#ifndef __HTTPSERVER_H__
#define __HTTPSERVER_H__

#include <inttypes.h>

class Connection;
class ConnectionPool;
class HttpRequest;
class HttpRequestParser;
class WiFi;

class HttpServer
{
private:
	Connection *currentConnection;
	ConnectionPool& connectionPool;
	WiFi& wifi;
	HttpRequest *request;
	HttpRequestParser *requestParser;
	bool connectionHandled;
public:
	HttpServer(ConnectionPool& _connectionPool, WiFi& wifi);
	~HttpServer();

	void work();
	void requestHandled();
	bool pendingRequest();
	Connection *startConnection(uint8_t connectionId);
	bool isRequestAvailable();
	HttpRequest* getRequest();

private:
	HttpServer( const HttpServer &c );
	HttpServer& operator=( const HttpServer &c );

};

#endif //__HTTPSERVER_H__
