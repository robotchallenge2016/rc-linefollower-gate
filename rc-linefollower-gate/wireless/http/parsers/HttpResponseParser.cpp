#include "HttpResponseParser.h"

HttpResponseParser::HttpResponseParser() 
	: isStatusParsed(false), lineIdx(0), statusIdx(0), statusCode(0)
{

}

HttpResponseParser::~HttpResponseParser()
{

}

void HttpResponseParser::parse(uint8_t data)
{
	if(!isStatusParsed)
	{
		if(lineIdx <= 14)
		{
			++lineIdx;
		}
		else
		{
			switch(statusIdx)
			{
				case 0:
				{
					statusCode = 100 * (data - asciiOffset);
					statusIdx = 1;
					break;
				}
				case 1:
				{
					statusCode += 10 * (data - asciiOffset);
					statusIdx = 2;
					break;
				}
				case 2:
				{
					statusCode += (data - asciiOffset);
					statusIdx = 0;
					isStatusParsed = true;
					break;
				}
			}
		}
	}
	if(isStatusParsed)
	{
		success();
	}
}

uint16_t HttpResponseParser::getStatusCode()
{
	return statusCode;
}
