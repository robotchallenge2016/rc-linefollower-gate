#include "HttpRequestParser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

HttpRequestParser::HttpRequestParser()
	: isCr(false), isLf(false), isDouble(false), contentCounter(0), contentLength(0), body(NULL)
{

}

HttpRequestParser::~HttpRequestParser()
{
}

void HttpRequestParser::parse(uint8_t data)
{
	if(body)
	{
		body[contentCounter++] = data;
		if(contentCounter >= contentLength)
		{
			body[contentLength] = '\0';
			contentCounter = 0;
			success();
		}
	}
	else if(data != '\r' && data != '\n')
	{
		line[lineIdx++] = data;
		isCr = isLf = false;
	}
	else if(data == '\n')
	{
		if(!contentLength)
		{
			const char* header = "Content-Length:";
			uint8_t headerSize = strlen(header);
			if(strncmp((const char*)line, header, headerSize) == 0)
			{
				contentLength = atoi((const char*)(line + headerSize));
			}
		}
		lineIdx = 0;

		if(isLf)
		{
			if(body)
			{	
				delete body;
				body = NULL;	
			}
			body = new uint8_t[contentLength + 1];
			body[contentLength] = '\0';
			if(contentLength == 0)
			{
				success();
			}
		}
		isLf = true;
	}
}

uint8_t* HttpRequestParser::getBody()
{
	return body;
}

uint32_t HttpRequestParser::getContentLength()
{
	return contentLength;
}