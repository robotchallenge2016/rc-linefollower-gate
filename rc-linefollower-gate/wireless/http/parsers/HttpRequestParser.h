#ifndef __HTTPREQUESTPARSER_H__
#define __HTTPREQUESTPARSER_H__

#include "wireless/Parser.h"

class HttpRequestParser : public Parser
{
private:
	uint8_t line[128];
	uint8_t lineIdx;
	bool isCr;
	bool isLf;
	bool isDouble;
	uint16_t contentCounter;

	//Headers
	uint32_t contentLength;

	//Body
	uint8_t *body;
public:
	HttpRequestParser();
	~HttpRequestParser();

	void parse(uint8_t data);
	uint8_t *getBody();
	uint32_t getContentLength();

private:
	HttpRequestParser( const HttpRequestParser &c );
	HttpRequestParser& operator=( const HttpRequestParser &c );

};

#endif //__HTTPREQUESTPARSER_H__
