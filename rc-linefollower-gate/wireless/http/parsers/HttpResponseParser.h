#ifndef __HTTPRESPONSEPARSER_H__
#define __HTTPRESPONSEPARSER_H__

#include "wireless/Parser.h"

#include <inttypes.h>

class HttpResponseParser : public Parser
{
private:
	
	bool isStatusParsed;
	uint8_t lineIdx;
	uint8_t statusIdx;

	uint16_t statusCode;
public:
	HttpResponseParser();
	~HttpResponseParser();

	void parse(uint8_t data);
	uint16_t getStatusCode();

private:
	HttpResponseParser( const HttpResponseParser &c );
	HttpResponseParser& operator=( const HttpResponseParser &c );

};

#endif //__HTTPRESPONSEPARSER_H__
