#ifndef __HTTPCLIENT_H__
#define __HTTPCLIENT_H__

class Command;
class Connection;
class ConnectionPool;
class HttpRequest;
class HttpResponse;
class HttpResponseParser;
class WiFi;

class HttpClient
{
private:
	ConnectionPool& pool;
	WiFi& wifi;
	Connection *currentConnection;
	HttpResponse *response;
	HttpResponseParser *responseParser;

public:
	HttpClient(ConnectionPool& pool, WiFi& wifi);
	~HttpClient();

	void work();

	void send(HttpRequest& request);
	bool pendingRequest();
	Connection* getCurrentConnection();
	bool isResponseAvailable();
	HttpResponse* getResponse();

private:
	HttpClient( const HttpClient &c );
	HttpClient& operator=( const HttpClient &c );

};

#endif //__HTTPCLIENT_H__
