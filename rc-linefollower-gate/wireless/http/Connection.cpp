#include "Connection.h"

#include "wireless/http/parsers/HttpRequestParser.h"
#include "wireless/http/parsers/HttpResponseParser.h"

#include <stdlib.h>
#include <string.h>

Connection::Connection(uint8_t _id) : id(_id), parser(NULL), closed(false), closedIdx(0)
{
	maxClosedIdx = strlen(closedText);
}

Connection::~Connection()
{

}

uint8_t Connection::getId()
{
	return id;
}

void Connection::responseChunk(uint8_t chunk)
{
	if(chunk == closedText[closedIdx])
	{
		++closedIdx;
		if(closedIdx == maxClosedIdx)
		{
			closedIdx = 0;
			closed = true;
		}
	}
	if(parser)
	{
		parser->parse(chunk);
	}
}

void Connection::setParser(Parser* parser)
{
	this->parser = parser;
}

void Connection::obtained()
{
	closed = false;
}

void Connection::released()
{

}

void Connection::close()
{
	closed = true;
}

bool Connection::isClosed()
{
	return closed;
}