#include "HttpRequest.h"

#include <stdio.h>
#include <string.h>

HttpRequest::HttpRequest(const char* method, const char* url, const char* body)
{
	contentLength = strlen(body);
	snprintf(buffer, HTTP_BUFFER_SIZE, 
	"%s %s HTTP/1.1\r\n"
	"Accept: application/json\r\n"
	"Authorization: Basic ODMxZTM0N2MtZDljMS00NWM1LWEzNDctZDZlYmJmZmM4ZWQ1OkxpbmVGb2xsb3dlckdhdGU=\r\n"
	"Connection: close\r\n"
	"Content-Length: %lu\r\n"
	"Content-Type: application/json;charset=UTF-8\r\n"
	"Host: lf.gate.pl\r\n"
	"\r\n"
	"%s"
	"\r\n", method, url, contentLength, body);
}

HttpRequest::HttpRequest(const char* _body, uint32_t _contentLength)
	: contentLength(_contentLength)
{
	this->body = new char[contentLength + 1];
	strncpy(this->body, _body, contentLength + 1);
}

HttpRequest::~HttpRequest()
{
	delete body;
}

const char* HttpRequest::toWire()
{
	return buffer;
}

const char* HttpRequest::getBody()
{
	return body;
}

uint32_t HttpRequest::getContentLength()
{
	return contentLength;
}
