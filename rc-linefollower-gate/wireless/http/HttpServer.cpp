#include "HttpServer.h"

#include "wireless/http/Connection.h"
#include "wireless/http/ConnectionPool.h"
#include "wireless/http/HttpRequest.h"
#include "wireless/http/parsers/HttpRequestParser.h"
#include "wireless/WiFi.h"

#include <stdlib.h>

HttpServer::HttpServer(ConnectionPool& _connectionPool, WiFi& _wifi)
	: 
	currentConnection(NULL), 
	connectionPool(_connectionPool), 
	wifi(_wifi), 
	request(NULL),
	connectionHandled(false)
{
	requestParser = new HttpRequestParser();
}

HttpServer::~HttpServer()
{
	if(requestParser)
	{	
		delete requestParser;
	}
}

void HttpServer::work()
{
	if(currentConnection)
	{
		if(requestParser->isFinished())
		{
			wifi.send(*currentConnection, 
			"HTTP/1.1 200 OK\r\n"
			"Access-Control-Allow-Origin: *\r\n"
			"Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept\r\n"
			"Access-Control-Allow-Method: *\r\n"
			"Connection: close\r\n"
			"\r\n");
			connectionPool.release(currentConnection->getId());
			currentConnection->close();
			currentConnection = NULL;
		}
	}
}

void HttpServer::requestHandled()
{
	requestParser->rewind();
}

bool HttpServer::pendingRequest()
{
	return (currentConnection != NULL);
}
	
Connection* HttpServer::startConnection(uint8_t connectionId)
{
	if(request)
	{
		delete request;
		request = NULL;
	}
	if(requestParser)
	{
		delete requestParser;
		requestParser = new HttpRequestParser();
	}
	currentConnection = connectionPool.obtain(connectionId);
	currentConnection->setParser(requestParser);
	return currentConnection;
}

bool HttpServer::isRequestAvailable()
{
	return requestParser->isFinished();
}

HttpRequest* HttpServer::getRequest()
{
	if(request)
	{
		return request;
	}
	request = new HttpRequest((const char *)requestParser->getBody(), requestParser->getContentLength());
	return request;
}