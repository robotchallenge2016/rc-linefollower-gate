#include "HttpClient.h"

#include "wireless/http/Connection.h"
#include "wireless/http/ConnectionPool.h"
#include "wireless/http/HttpRequest.h"
#include "wireless/http/HttpResponse.h"
#include "wireless/http/parsers/HttpResponseParser.h"
#include "wireless/WiFi.h"

HttpClient::HttpClient(ConnectionPool& _pool, WiFi& _wifi) 
	: pool(_pool), wifi(_wifi), currentConnection(NULL), response(NULL), responseParser(NULL)
{

}

HttpClient::~HttpClient()
{
}

void HttpClient::work()
{
	if(currentConnection)
	{
		if(responseParser->isFinished() && response == NULL)
		{
			response = new HttpResponse(*responseParser);
		}
		if(currentConnection->isClosed())
		{
			pool.release(currentConnection->getId());
			currentConnection = NULL;			
		}
	}
}

void HttpClient::send(HttpRequest& request)
{
	if(response)
	{
		delete response;
		response = NULL;
	}
	if(responseParser)
	{
		delete responseParser;
	}
	responseParser = new HttpResponseParser();
	currentConnection = pool.obtain(4);
	currentConnection->setParser(responseParser);
	wifi.startAndSend(*currentConnection, request.toWire());
}

bool HttpClient::pendingRequest()
{
	return (currentConnection != NULL);
}

Connection* HttpClient::getCurrentConnection()
{
	return currentConnection;
}

bool HttpClient::isResponseAvailable()
{
	return responseParser->isFinished();
}

HttpResponse* HttpClient::getResponse()
{
	if(!response && isResponseAvailable())
	{
		response = new HttpResponse(*responseParser);
	}
	return response;
}