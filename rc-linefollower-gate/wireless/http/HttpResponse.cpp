#include "HttpResponse.h"

#include "wireless/http/parsers/HttpResponseParser.h"

HttpResponse::HttpResponse(HttpResponseParser& parser)
{
	statusCode = parser.getStatusCode();
}

HttpResponse::HttpResponse( const HttpResponse &c )
{
	statusCode = c.statusCode;
}

HttpResponse::~HttpResponse()
{

}

uint16_t HttpResponse::getStatusCode()
{
	return statusCode;
}
