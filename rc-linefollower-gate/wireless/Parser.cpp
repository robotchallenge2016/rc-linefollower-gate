#include "Parser.h"

Parser::Parser() : state(NOT_STARTED)
{

}

Parser::~Parser()
{

}

void Parser::rewind()
{
	changeStateTo(NOT_STARTED);
}

void Parser::inProgress()
{
	changeStateTo(IN_PROGRESS);
}

void Parser::success()
{
	changeStateTo(SUCCESS);
}

void Parser::error()
{
	changeStateTo(ERROR);
}

bool Parser::isFinished()
{
	return (state == ERROR) || (state == SUCCESS);
}

bool Parser::isSuccess()
{
	return (state == SUCCESS);
}

bool Parser::isError()
{
	return (state == ERROR);
}

void Parser::changeStateTo(ParsingState state)
{
	this->state = state;
}