#ifndef __COMMANDFACTORY_H__
#define __COMMANDFACTORY_H__

#include <inttypes.h>

class Parser;
class Command;

class CommandFactory
{
private:
	Command* atCmd;
	Command* rstCmd;
	Command* cwmodeCmd;
	Command* cwjapCmd;
	Command* cifsrCmd;
	Command* cipmuxCmd;
	Command* cipserverCmd;

	Command* cipStartCmd;
	Command* cipSendCmd;
	Command* dataCmd;
	Command* cipCloseCmd;

	Parser *okParser;
public:
	CommandFactory();
	~CommandFactory();

	Command* at();
	Command* rst();
	Command* cwmode();
	Command* cwjap();
	Command* cifsr();
	Command* cipmux();
	Command* cipserver();

	Command* cipStart(uint8_t id, uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint16_t port);
	Command* cipSend(uint8_t id, uint16_t length);
	Command* cipClose(uint8_t id);
	Command* data(const char* data);

private:
	CommandFactory( const CommandFactory &c );
	CommandFactory& operator=( const CommandFactory &c );

};

#endif //__COMMANDFACTORY_H__
