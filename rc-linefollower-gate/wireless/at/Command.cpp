#include "Command.h"

#include "wireless/WiFi.h"
#include "wireless/Parser.h"

#include <string.h>

Command::Command(const char* c, Parser *parser)
{
	uint16_t length = strlen(c);
	cmd = new char[length + 1];
	strncpy(cmd, c, length + 1);

	this->parser = parser;
}

Command::~Command()
{
	delete cmd;
}

void Command::send()
{
	char* iterator = cmd;
	while(*iterator)
	{
		while(!USART_TXBuffer_FreeSpace(&USART_WIFI_data));
		USART_TXBuffer_PutByte(&USART_WIFI_data, *iterator);
		++iterator;
	}
	USART_TXBuffer_PutByte(&USART_WIFI_data, '\r');
	USART_TXBuffer_PutByte(&USART_WIFI_data, '\n');
}

void Command::responseChunk(uint8_t chunk)
{
	parser->parse(chunk);
}

bool Command::isFulfilled()
{
	return parser->isFinished();
}

bool Command::isSuccess()
{
	return parser->isSuccess();
}

bool Command::isError()
{
	return parser->isError();
}
