#include "CommandDispatcher.h"

#include "wireless/at/Command.h"

#include <stdlib.h>

CommandDispatcher commandDispatcher;

CommandDispatcher::CommandDispatcher()
	: inProgress(NULL)
{
}

CommandDispatcher::~CommandDispatcher()
{
}

void CommandDispatcher::send(Command* cmd)
{
	inProgress = cmd;
	cmd->send();
}

void CommandDispatcher::responseChunk(uint8_t chunk)
{
	inProgress->responseChunk(chunk);
	if(inProgress->isFulfilled())
	{
		inProgress = NULL;
	}
}

bool CommandDispatcher::isInProgress()
{
	return inProgress != NULL;
}