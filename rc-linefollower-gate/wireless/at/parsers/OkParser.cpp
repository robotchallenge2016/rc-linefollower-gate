#include "OkParser.h"

OkParser::OkParser() : index(0)
{

}

OkParser::~OkParser()
{

}

void OkParser::parse(uint8_t data)
{
	inProgress();
	switch(index)
	{
		case 0:
		{
			validate(data, 'O', 1);
			break;
		}
		case 1:
		{
			validate(data, 'K', 2);
			break;
		}
		case 2:
		{
			validate(data, '\r', 3);
			break;
		}
		case 3:
		{
			validate(data, '\n', 0);
			success();
			break;
		}
	}
}

void OkParser::validate(uint8_t data, uint8_t expectedValue, uint8_t nextIndex)
{
	if(data == expectedValue)
	{
		index = nextIndex;
	}
	else
	{
		index = 0;
	}
}