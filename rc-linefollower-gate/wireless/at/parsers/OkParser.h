#ifndef __OKPARSER_H__
#define __OKPARSER_H__

#include <inttypes.h>

#include "wireless/Parser.h"

class OkParser : public Parser
{
private:
	uint8_t index;
//functions
public:
	OkParser();
	~OkParser();

	void parse(uint8_t data);
private:
	OkParser( const OkParser &c );
	OkParser& operator=( const OkParser &c );

	void validate(uint8_t data, uint8_t expectedValue, uint8_t nextIndex);
};

#endif //__OKPARSER_H__
