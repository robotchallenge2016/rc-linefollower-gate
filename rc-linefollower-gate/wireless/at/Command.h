#ifndef __COMMAND_H__
#define __COMMAND_H__

#include <inttypes.h>

class Parser;

class Command
{
private:
	char* cmd;
	Parser *parser;

public:
	Command(const char* cmd, Parser *parser);
	~Command();

	void send();
	void responseChunk(uint8_t chunk);

	bool isFulfilled();
	bool isSuccess();
	bool isError();

private:
	Command( const Command &c );
	Command& operator=( const Command &c );
};

#endif //__COMMAND_H__
