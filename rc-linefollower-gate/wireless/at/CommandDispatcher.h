#ifndef __COMMANDDISPATCHER_H__
#define __COMMANDDISPATCHER_H__

#include <inttypes.h>

class Command;
class CommandDispatcher;
extern CommandDispatcher commandDispatcher;

class CommandDispatcher
{
private:
	Command *inProgress;
public:
	CommandDispatcher();
	~CommandDispatcher();

	void send(Command* cmd);
	void responseChunk(uint8_t chunk);
	bool isInProgress();

private:
	CommandDispatcher( const CommandDispatcher &c );
	CommandDispatcher& operator=( const CommandDispatcher &c );

};

#endif //__COMMANDDISPATCHER_H__
