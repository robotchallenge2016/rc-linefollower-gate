#include "CommandFactory.h"

#include "wireless/at/Command.h"
#include "wireless/at/parsers/OkParser.h"

#include <stdio.h>

CommandFactory::CommandFactory()
{
	okParser = new OkParser();

	atCmd = new Command("AT", okParser);
	rstCmd = new Command("AT+RST", okParser);
	cwmodeCmd = new Command("AT+CWMODE=3", okParser);
	cwjapCmd = new Command("AT+CWJAP=\"DOM\",\"WWII19391945\"", okParser);
	//cwjapCmd = new Command("AT+CWJAP=\"HTC Portable Hotspot 12F6\",\"CFA5DBEF35AFE\"", okParser);
	cifsrCmd = new Command("AT+CIFSR", okParser);
	cipmuxCmd = new Command("AT+CIPMUX=1", okParser);
	cipserverCmd = new Command("AT+CIPSERVER=1,80", okParser);
}

CommandFactory::~CommandFactory()
{

}

Command* CommandFactory::at()
{
	return atCmd;
}

Command* CommandFactory::rst()
{
	return rstCmd;
}

Command* CommandFactory::cwmode()
{
	return cwmodeCmd;
}

Command* CommandFactory::cwjap()
{
	return cwjapCmd;
}

Command* CommandFactory::cifsr()
{
	return cifsrCmd;
}

Command* CommandFactory::cipmux()
{
	return cipmuxCmd;
}

Command* CommandFactory::cipserver()
{
	return cipserverCmd;
}

Command* CommandFactory::cipStart(uint8_t id, uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint16_t port)
{
	char cmd[64];
	snprintf(cmd, 64, "AT+CIPSTART=%u,\"TCP\",\"%u.%u.%u.%u\",%u", id, a, b, c, d, port);
	if(cipStartCmd)
	{
		delete cipStartCmd;
	}
	cipStartCmd = new Command(cmd, okParser);
	return cipStartCmd;
}

Command* CommandFactory::cipSend(uint8_t id, uint16_t length)
{
	char cmd[64];
	snprintf(cmd, 64, "AT+CIPSEND=%u,%u", id, length);
	if(cipStartCmd)
	{
		delete cipSendCmd;
	}
	cipSendCmd = new Command(cmd, okParser);
	return cipSendCmd;
}

Command* CommandFactory::cipClose(uint8_t id)
{
	char cmd[64];
	snprintf(cmd, 64, "AT+CIPCLOSE=%u", id);
	if(cipCloseCmd)
	{
		delete cipCloseCmd;
	}
	cipCloseCmd = new Command(cmd, okParser);
	return cipCloseCmd;
}

Command* CommandFactory::data(const char* data)
{
	if(dataCmd)
	{
		delete dataCmd;
	}
	dataCmd = new Command(data, okParser);
	return dataCmd;
}