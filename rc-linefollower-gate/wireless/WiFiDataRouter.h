#ifndef __WIFIDATAROUTER_H__
#define __WIFIDATAROUTER_H__

#include <inttypes.h>

class Connection;
class HttpClient;
class HttpServer;
class ConnectionPool;
class WiFi;

class WiFiDataRouter
{
private:
	HttpClient& httpClient;
	HttpServer& httpServer;
	ConnectionPool& connectionPool;
	WiFi& wifi;

	Connection* currentConnection;
	uint8_t connectionMarkerIdx;
	uint8_t maxConnectionMarkerIdx;
	bool connectionDetected;

	const char *connectionMarker;

	const uint8_t asciiOffset = 48;

public:
	WiFiDataRouter(HttpClient& httpClient, HttpServer& httpServer, ConnectionPool& connectionPool, WiFi& wifi);
	~WiFiDataRouter();

	void route();
private:
	WiFiDataRouter( const WiFiDataRouter &c );
	WiFiDataRouter& operator=( const WiFiDataRouter &c );

};

#endif //__WIFIDATAROUTER_H__
