#include "WiFiDataRouter.h"

#include "peripherals/Serial.h"

#include "wireless/WiFi.h"
#include "wireless/at/Command.h"
#include "wireless/at/CommandDispatcher.h"
#include "wireless/http/Connection.h"
#include "wireless/http/ConnectionPool.h"
#include "wireless/http/HttpClient.h"
#include "wireless/http/HttpServer.h"

#include "utils/usart_driver.h"


WiFiDataRouter::WiFiDataRouter(HttpClient& _httpClient, HttpServer& _httpServer, ConnectionPool& _connectionPool, WiFi& _wifi) 
	: 
	httpClient(_httpClient),
	httpServer(_httpServer),
	connectionPool(_connectionPool), wifi(_wifi), 
	currentConnection(NULL), connectionMarkerIdx(0), maxConnectionMarkerIdx(5),
	connectionDetected(false)
{
	connectionMarker = "+IPD,";
}

WiFiDataRouter::~WiFiDataRouter()
{
}

void WiFiDataRouter::route()
{
	if(USART_RXBufferData_Available(&USART_WIFI_data))
	{
		uint8_t chunk = USART_RXBuffer_GetByte(&USART_WIFI_data);

		if(currentConnection && currentConnection->isClosed())
		{
			connectionMarkerIdx = 0;
			currentConnection = NULL;
		}

		if(connectionDetected)
		{
			uint8_t connectionId = chunk - asciiOffset;
			if(httpClient.pendingRequest() && httpClient.getCurrentConnection()->getId() == connectionId)
			{
				currentConnection = httpClient.getCurrentConnection();
			}
			else if(!httpServer.pendingRequest())
			{
				currentConnection = httpServer.startConnection(connectionId);
			}
			else
			{
				//ignore
			}
			connectionDetected = false;
		}

		if(commandDispatcher.isInProgress())
		{
			commandDispatcher.responseChunk(chunk);
		}
		else if(currentConnection)
		{
			currentConnection->responseChunk(chunk);
		}
		else if((connectionMarkerIdx > 0 && connectionMarkerIdx < maxConnectionMarkerIdx) ||
				(chunk == '+' && connectionMarkerIdx == 0))
		{
			if(chunk == connectionMarker[connectionMarkerIdx])
			{
				++connectionMarkerIdx;
			}
			if(connectionMarkerIdx == maxConnectionMarkerIdx)
			{
				connectionDetected = true;
			}
		}
		else
		{
			wifi.parse(chunk);
		}

		USART_TXBuffer_PutByte(&USART_PC_data, chunk);
	}
}
