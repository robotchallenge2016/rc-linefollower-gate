/*
 * delays.h
 *
 * Created: 2015-07-18 17:03:22
 *  Author: Maciek
 */ 


#ifndef DELAYS_H
#define DELAYS_H

#include <inttypes.h>

#ifdef __cplusplus
extern "C"{
#endif
	void delayms(uint32_t delay);
	void delay1ms();
	void delay10ms();
	void delay50ms();
	void delay100ms();
	void delay200ms();
	void delay300ms();
	void delay400ms();
	void delay500ms();
	void delay600ms();
	void delay700ms();
	void delay800ms();
	void delay900ms();
	void delay1s();
#ifdef __cplusplus
}
#endif


#endif /* DELAYS_H */