/*
 * delays.c
 *
 * Created: 2015-07-18 17:06:06
 *  Author: Maciek
 */ 

#include "delays.h"
#include <util/delay.h>

void delayms(uint32_t delay)
{
	for(uint32_t i = 0; i < delay; ++i)
	{
		_delay_ms(1);
	}	
}

void delay1ms()
{
	_delay_ms(1);
}
void delay10ms()
{
	delayms(10);
}

void delay50ms()
{
	delayms(50);
}

void delay100ms()
{
	delayms(100);
}

void delay200ms()
{
	delayms(200);
}

void delay300ms()
{
	delayms(300);
}

void delay400ms()
{
	delayms(400);
}

void delay500ms()
{
	delayms(500);
}

void delay600ms()
{
	delayms(600);
}

void delay700ms()
{
	delayms(700);
}

void delay800ms()
{
	delayms(800);
}

void delay900ms()
{
	delayms(900);
}

void delay1s()
{
	delayms(1000);
}