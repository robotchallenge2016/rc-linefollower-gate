#ifndef APPLICATION_H_
#define APPLICATION_H_

class CommandFactory;
class ConnectionPool;
class HttpClient;
class HttpServer;
class Program;
class WiFi;
class WiFiDataRouter;

extern CommandFactory commandFactory;
extern ConnectionPool connectionPool;
extern HttpClient httpClient;
extern HttpServer httpServer;
extern WiFi wifi;
extern Program program;
extern WiFiDataRouter dataRouter;

#endif /* APPLICATION_H_ */