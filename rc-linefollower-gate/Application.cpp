#include "Application.h"

#include "wireless/at/CommandFactory.h"
#include "wireless/http/ConnectionPool.h"
#include "wireless/http/HttpClient.h"
#include "wireless/http/HttpServer.h"
#include "Program.h"
#include "wireless/WiFi.h"
#include "wireless/WiFiDataRouter.h"

CommandFactory commandFactory;
ConnectionPool connectionPool;
WiFi wifi(commandFactory);

HttpClient httpClient(connectionPool, wifi);
HttpServer httpServer(connectionPool, wifi);
Program program(httpClient, httpServer, wifi);
WiFiDataRouter dataRouter(httpClient, httpServer, connectionPool, wifi);

