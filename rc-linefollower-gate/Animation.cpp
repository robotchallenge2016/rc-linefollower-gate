#include "Animation.h"

#include "peripherals/SegmentDisplay.h"

Animation::Animation(SegmentDisplay &segmentDisplay) 
	: segmentDisplay(segmentDisplay), state(0), maxStates(16)
{

}

Animation::~Animation()
{
	
}

void Animation::play()
{
	uint8_t *values = slides[state];
	segmentDisplay.writeAndDisplay(values, 6);
	segmentDisplay.turnOn();
	++state;
	state = state % maxStates;
}

void Animation::success()
{
	segmentDisplay.writeAndDisplay(successData, 6);
	segmentDisplay.switchToGreen();
	segmentDisplay.turnOn();
}

void Animation::error()
{
	segmentDisplay.writeAndDisplay(errorData, 6);
	segmentDisplay.switchToRed();
	segmentDisplay.turnOn();
}

void Animation::boot()
{
	segmentDisplay.writeAndDisplay(bootData, 6);
	segmentDisplay.switchToRed();
	segmentDisplay.turnOn();
}
