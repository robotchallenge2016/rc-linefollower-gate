#ifndef __PROGRAM_H__
#define __PROGRAM_H__

#include <inttypes.h>

class Animation;
class HttpClient;
class HttpServer;
class WiFi;

enum ProgramState
{
	WAITING_FOR_ROBOT,
	WAITING_FOR_RACE_START,
	WAITING_FOR_RACE_STOP,
	SEND_RESULT_TO_SERVER,
	WAITING_FOR_SERVER_RESPONSE,
	SHOW_SUCCESS,
	SHOW_ERROR,
	WAITING_AFTER_RACE
};

class Program
{
private:
	ProgramState state;
	Animation *animation;
	HttpClient &httpClient;
	HttpServer &httpServer;
	WiFi &wifi;

	uint16_t threshold;
	uint16_t tickCounter;
	uint16_t waitCounter;
	uint16_t waitForResponseCounter;
	uint16_t successCounter;
	uint16_t errorCounter;

	bool isManualStart;
	bool isAutomaticStart;

	const char *competitionGuidId = "competitionGuid";
	char competitionGuid[37];
	const char *robotGuidId = "robotGuid";
	char robotGuid[37];

public:
	Program(HttpClient& httpClient, HttpServer& httpServer, WiFi& wifi);
	~Program();

	void run();
	void tick();

protected:
private:
	Program( const Program &c );
	Program& operator=( const Program &c );

	void initializeHardware();

	bool checkRightLaser();
	bool checkLeftLaser();

	bool manualStart();
	bool automaticStart();
	void forwardSerialDataToWiFi();

	void waitForRobot();
	void waitForRaceStart();
	void waitForRaceStop();
	void sendResultToServer();
	void waitForResponseFromServer();
	void showSuccess();
	void showError();
	void waitAfterRace();
};

#endif //__PROGRAM_H__
